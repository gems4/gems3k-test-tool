#!/bin/bash
# Installing dependencies needed to build test-diff

# nlohmann/json library v. 3.6.1 or up
# if not installed in /usr/local/include/nlohmann)
test -f /usr/local/include/nlohmann/json.hpp || {

        # Getting nlohmann/json library
        mkdir -p ~/code && \
                cd ~/code && \
                git clone https://github.com/nlohmann/json.git && \
                cd json && \
                mkdir -p build && \
                cd build && \
                cmake .. -DCMAKE_BUILD_TYPE=Release -DJSON_BuildTests=OFF -DJSON_MultipleHeaders=ON && \
                sudo make install

        # Removing generated build files
        cd ~ && \
                 rm -rf ~/code
}

