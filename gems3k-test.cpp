//--------------------------------------------------------------------
// $Id: diff-tool.cpp
//
//
// Copyright (C) 2022 D.Kulik, S.Dmytriyeva
// <GEMS Development Team, mailto:gems2.support@psi.ch>
//
// This file is part of the GEMS3K code for thermodynamic modelling
// by Gibbs energy minimization <http://gems.web.psi.ch/GEMS3K/>
//
// GEMS3K is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.

// GEMS3K is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GEMS3K code. If not, see <http://www.gnu.org/licenses/>
//-------------------------------------------------------------------

#ifdef OVERFLOW_EXCEPT
#ifdef __linux__
#include <cfenv>
#elif _MSC_VER
#include <float.h>
#else
#include <cfenv>
#endif
#endif

#include <iostream>
#include <memory>
#include "node.h"
#include "v_service.h"
#include "jsonioDiff/diffkvasjson.h"

#include <regex>
#include <filesystem>
namespace fs = std::filesystem;

/// Descripton of data to execute GEMS IPN test tool.
class GEMS3KTestData
{

public:

    /// IPM work structure file path&name
    std::string ipmfiles_lst_dir;

    /// Number of allocated nodes
    long int nIV = 1;

    /// Generate Multi dump (possible debugging print out)
    bool dump_multi = false;

    /// Comparator used to compare two values
    jsoniodiff::Comparator compare_method;

    /// Path to JSON structured document describing data to compare
    std::string rules_json_document_file;

    /// Loaded template  - a json  or key-value format file
    std::shared_ptr<jsoniodiff::JsonFile> templ_file;

    /// Loaded source  - a json  or key-value format file
    std::shared_ptr<jsoniodiff::JsonFile> calc_file;

    int extract_args( int argc, char* argv[], std::string& test_tasks_dir );
    void show_usage( const std::string &name );
    bool compare_files(GEMS3KGenerator::IOModes type, const std::string& template_dbr, const std::string& result_dbr);

};

// true if the same data in dbr
bool test_diff_task(const std::string& task_lst_path, GEMS3KTestData& diff_data );

// -a  0.1e-10 -d data
// -rd  data-GUI/dbr_diff.json -a  1e-7 -d data-GUI
// -rd  data-FUN/dbr_diff.json -a  1e-7 -d data-FUN

// Testing the difference after IPN recalculation
int main( int argc, char* argv[] )
{

#if  defined(OVERFLOW_EXCEPT)
#ifdef __linux__
    feenableexcept (FE_DIVBYZERO|FE_OVERFLOW|FE_UNDERFLOW);
#elif _MSC_VER
    _clearfp();
    _controlfp(_controlfp(0, 0) & ~(_EM_INVALID | _EM_ZERODIVIDE | _EM_OVERFLOW),
               _MCW_EM);
#else

#endif
#endif

    gems3k_update_loggers(false, "gems3k_test.log", spdlog::level::info);

    try{
        std::string test_tasks_dir = "data";
        GEMS3KTestData diff_data;

        if( diff_data.extract_args( argc, argv, test_tasks_dir )) {
            return 1;
        }
        fs::path data_dir(test_tasks_dir);
        std::regex lst_rx(".*\\-dat.lst");
        bool the_same = true;
        int n_failed=0, n_nrojects = 0;

        if( !fs::exists(data_dir) ) {
            std::cout << data_dir << " directory does not exist." << std::endl;
            return 1;
        }

        for(auto& d: fs::directory_iterator(data_dir))
        {
            if (fs::is_directory(d.path()))
            {
                std::string task_dir = test_tasks_dir+"/";
                task_dir += d.path().filename();
                fs::path dir(task_dir);

                for(auto& f: fs::directory_iterator(dir))
                {
                    std::string file = f.path().filename();
                    if ( std::regex_match( file , lst_rx ) )
                    {
                        auto file_path = task_dir+"/"+file;
                        auto passed = test_diff_task(file_path, diff_data);
                        if( passed ) {
                            std::cout <<  "\033[32m  Pass: \033[0m" <<  file_path <<  std::endl;
                        }
                        else {
                            n_failed++;
                            std::cout <<  "\033[31m  Fail: \033[0m" <<  file_path <<  std::endl;
                        }
                        the_same &= passed;
                        n_nrojects++;
                    }
                }
            }
        }

        std::cout << " ====================================== " << std::endl;
        if( the_same ) {
            std::cout << "\033[32m [ PASSED ] \033[0m" << n_nrojects << " projects" << std::endl;
        }
        else
        {
            std::cout << "\033[31m [ FAILED ] \033[0m" << n_failed << " of " <<  n_nrojects << " projects" << std::endl;
        }

        return 0;
    }
    catch(TError& err)
    {
        std::cout  << err.title << err.mess << std::endl;
    }
    catch(std::exception& e)
    {
        std::cout  << "std::exception: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cout  << "unknown exception" << std::endl;
    }
    return -1;
}

// true if the same data in dbr
bool test_diff_task( const std::string& task_lst_path, GEMS3KTestData& diff_data )
{
    std::cout << "Testing: " << task_lst_path << std::endl;
    GEMS3KGenerator input_data(task_lst_path);
    std::string template_dbr = input_data.get_dbr_path(0);
    std::string calculated_dbr = input_data.GEMS3KGenerator::get_path("Calculated-dbr.")+input_data.extension();
    auto fmode = input_data.files_mode();

    // Creates TNode structure instance accessible through the "node" pointer
    std::shared_ptr<TNode> node( new TNode() );

    // Initialization of GEMS3K_diff_json internal data by reading  extensionfiles
    //     whose names are given in the input_system_file_list_name
    if( node->GEM_init( task_lst_path.c_str() ))
    {
        std::cout << "Error occured during reading the files: " << task_lst_path << std::endl;
        return false;
    }

    // Getting direct access to work node DATABR structure which exchanges the
    // data with GEM IPM3 (already filled out by reading the DBR input file)
    DATABR* dBR = node->pCNode();

    // Asking GEM to run with automatic initial approximation
    dBR->NodeStatusCH = NEED_GEM_AIA;
    if( diff_data.dump_multi ) {
        node->GEM_print_ipm( input_data.get_path("BeforeRun-MultiDump.txt").c_str() );
    }

    // re-calculating equilibri_diff_jsonum by calling GEMS3K, getting the status back
    long NodeStatusCH = node->GEM_run( false );
    if( NodeStatusCH == OK_GEM_AIA || NodeStatusCH == OK_GEM_SIA  )
    {
        // Writing results in default DBR file
        node->GEM_write_dbr( calculated_dbr.c_str(), fmode, true, false );
        if( diff_data.dump_multi ) {
            node->GEM_print_ipm( input_data.get_path("AfterRun-MultiDump.txt").c_str() );
        }
    }
    else
    {
        node->GEM_print_ipm( nullptr );
        std::cout << "GEM IPM did not converge properly: " << task_lst_path << std::endl;
        return false;
    }

    return diff_data.compare_files(fmode, calculated_dbr, template_dbr);
}


bool GEMS3KTestData::compare_files(GEMS3KGenerator::IOModes type, const std::string &result_dbr, const std::string &template_dbr)
{
    std::cout << "Compare file: " << result_dbr << " to template " <<  template_dbr << std::endl;
    if( type == GEMS3KGenerator::f_key_value ||
            type == GEMS3KGenerator::f_kv_thermofun )
    {
        templ_file = std::make_shared<jsoniodiff::KeyValueJsonFile>(template_dbr);
        calc_file = std::make_shared<jsoniodiff::KeyValueJsonFile>(result_dbr);
    }
    else
    {
        templ_file = std::make_shared<jsoniodiff::JsonFile>(template_dbr);
        calc_file = std::make_shared<jsoniodiff::JsonFile>(result_dbr);
    }

    calc_file->updateTemplateDiff( compare_method );
    if( !rules_json_document_file.empty() )
    {
        calc_file->updateTemplateDiff( rules_json_document_file );
    }
    templ_file->load_all();
    calc_file->load_all();
    return calc_file->compare_to(*templ_file, compare_method);
}

void GEMS3KTestData::show_usage( const std::string &name )
{
    std::cout << "Usage: " << name << " [ option(s) ] -d|--test-from TEST_DIR "
              << "\nTool for read, recalculate task and compare for init data\n"
              << "Options:\n"
              << "\t-h,\t--help\t\tshow this help message\n\n"
                 // method
              << "\t-b,\t--difference   \t\toutput all differences between template and source (default)\n"
              << "\t-c,\t--consist-in   \t\toutput all data from the template not consist in the source\n\n"
                 // epsilon
              << "\t-a,\t--approximately EPS\tapproximately equal for floating types\n"
              << "\t-e,\t--essentially EPS\tessentially equal for floating types\n"
              << "\t-at,\t--absolute-tolerance EPS\tabsolute tolerance equal for floating types(default)\n"
              << "\t-sd,\t--significant-digits EPS\tidentical significant digits equal for floating types\n"
              << "\t-lg,\t--log-of-values EPS\tidentical significant digits of log of values equal for floating types\n"
              << "\t-rd,\t--rules-document JSONF  \tstructured document describing data to compare\n\n"
              << "\t-md,\t--multi-dump\t\tMulti dump debugging print out\n\n"
              << std::endl;
}

int GEMS3KTestData::extract_args( int argc, char* argv[], std::string& test_tasks_dir )
{
    int i=0;
    std::string eps = std::to_string( std::numeric_limits<double>::epsilon());

    for( i = 1; i < argc; ++i)
    {
        std::string arg = argv[i];
        if ((arg == "-h") || (arg == "--help"))
        {
            show_usage( "diff-test" );;
            return 1;
        }
        else if ((arg == "-d") || (arg == "--test-from"))
        {
            if (i + 1 < argc) {
                test_tasks_dir = argv[++i];
            } else {
                std::cerr << "--test-from option requires one argument." << std::endl;
                return 1;
            }
        }
        else if ((arg == "-rd") || (arg == "--rules-document"))
        {
            if (i + 1 < argc) {
                rules_json_document_file = argv[++i];
            } else {
                std::cerr << "--rules-document option requires one argument." << std::endl;
                return 1;
            }
        }
        else if ((arg == "-b") || (arg == "--difference"))
        {
            compare_method.setMethod( jsoniodiff::Comparator::Difference );
        }
        else if ((arg == "-c") || (arg == "--consist-in"))
        {
            compare_method.setMethod( jsoniodiff::Comparator::ConsistIn );
        }
        else if ((arg == "-a") || (arg == "--approximately"))
        {
            if (i + 1 < argc) {
                eps = argv[++i];
            } else {
                std::cerr << "--approximately option requires one argument." << std::endl;
                return 1;
            }
            compare_method.setEpsilon( std::stod(eps), jsoniodiff::FloatCompareMethod::AprDiffMore );
        }
        else if ((arg == "-e") || (arg == "--essentially"))
        {
            if (i + 1 < argc) {
                eps = argv[++i];
            } else {
                std::cerr << "--essentially option requires one argument." << std::endl;
                return 1;
            }
            compare_method.setEpsilon( std::stod(eps), jsoniodiff::FloatCompareMethod::RelDiffMore );
        }
        else if ((arg == "-at") || (arg == "--absolute-tolerance"))
        {
            if (i + 1 < argc) {
                eps = argv[++i];
            } else {
                std::cerr << "----absolute-tolerance option requires one argument." << std::endl;
                return 1;
            }
            compare_method.setEpsilon( std::stod(eps), jsoniodiff::FloatCompareMethod::AbsDiffMore );
        }
        else if ((arg == "-sd") || (arg == "--significant-digits"))
        {
            if (i + 1 < argc) {
                eps = argv[++i];
            } else {
                std::cerr << "--significant-digits option requires one argument." << std::endl;
                return 1;
            }
            compare_method.setEpsilon( std::stod(eps), jsoniodiff::FloatCompareMethod::SigDigLess );
        }
        else if ((arg == "-lg") || (arg == "--log-of-values"))
        {
            if (i + 1 < argc) {
                eps = argv[++i];
            } else {
                std::cerr << "--log-of-values option requires one argument." << std::endl;
                return 1;
            }
            compare_method.setEpsilon( std::stod(eps), jsoniodiff::FloatCompareMethod::LogDigLess );
        }

    }

    if( !rules_json_document_file.empty() && !fs::exists(rules_json_document_file) ) {
        std::cout << rules_json_document_file << " file does not exist." << std::endl;
        return 1;
    }
    return 0;
}


// Generate data test from GUI test
// ./kva2json -j -c -i diff/data-GUI/Neutral-old/Neutral-dat.lst -e diff/data/Neutral-old/Neutral-dat.lst
// ./kva2json -j -c -i diff/data-GUI/Solvus1-json/series1-dat.lst -e diff/data/Solvus1-json/series1-dat.lst
// ./kva2json -j -c -i diff/data-GUI/Kaolinite-json/pHtitr-dat.lst -e diff/data/Kaolinite-json/pHtitr-dat.lst
// ./kva2json -t -c -i diff/data-GUI/Kaolinite-kv/pHtitr-dat.lst -e diff/data/Kaolinite-kv/pHtitr-dat.lst
