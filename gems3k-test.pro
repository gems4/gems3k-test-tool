#  qmake project file for the diff-test tool (part of GEMS3K standalone code)
# (c) 2022 GEMS Developer Team
 
TEMPLATE = app
LANGUAGE = C++
TARGET = gems3k-test
VERSION = 3.4.6

CONFIG -= qt
CONFIG -= warn_on
CONFIG += debug
CONFIG += console
CONFIG += c++17

DEFINES += IPMGEMPLUGIN
DEFINES += NODEARRAYLEVEL
DEFINES += USE_THERMOFUN
DEFINES += USE_THERMO_LOG
#DEFINES += USE_NLOHMANNJSON
DEFINES += OVERFLOW_EXCEPT  #compile with nan inf exceptions

!win32 {

DEFINES += __unix
QMAKE_CFLAGS += -pedantic -Wall -Wextra -Wwrite-strings -Werror

QMAKE_CXXFLAGS += -fPIC -Wall -Wextra -Wformat-nonliteral -Wcast-align -Wpointer-arith \
 -Wmissing-declarations -Winline \ # -Wundef \ #-Weffc++ \
 -Wcast-qual -Wshadow -Wwrite-strings -Wno-unused-parameter \
 -Wfloat-equal -pedantic -ansi

}

GEMS3K_CPP = ./gems3k/GEMS3K
DIFF_DIR = ./jsoniodiff
GEMS3K_H   = $$GEMS3K_CPP
JSONIODIFF_DIR = $$DIFF_DIR/src
JSONIODIFF_HEADERS_DIR = $$DIFF_DIR/include

DEPENDPATH += .
DEPENDPATH += $$GEMS3K_H
DEPENDPATH += $$JSONIODIFF_HEADERS_DIR

INCLUDEPATH += .
INCLUDEPATH += $$GEMS3K_H
INCLUDEPATH += $$JSONIODIFF_HEADERS_DIR

contains(DEFINES, USE_THERMOFUN) {

#ThermoFun_CPP   =  ../thermoFun/ThermoFun
#ThermoFun_H     =   $$ThermoFun_CPP
#DEPENDPATH += $$ThermoFun_H
#INCLUDEPATH += $$ThermoFun_H
#include($$ThermoFun_CPP/ThermoFun.pri)
#LIBS +=  -lChemicalFun
LIBS += -lThermoFun -lChemicalFun

}

QMAKE_LFLAGS +=
#QMAKE_CXXFLAGS += -Wall -Wno-unused
OBJECTS_DIR = obj

include($$GEMS3K_CPP/gems3k.pri)
include($$JSONIODIFF_DIR/jsoniodiff.pri)

SOURCES   +=   \
    gems3k-test.cpp

