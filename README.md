## GEMS3K-test

*GEMS3k-test* CLI utility is the test utility for GEM IPM calculations using the input bulk composition, temperature, pressure, and metastability constraints provided in the work instance of DATABR structure. It is compared the template result of GEMS IPM with currently calculated data.

As the test suite for testing the difference after [IPN recalculation](https://bitbucket.org/gems4/gems3k/src/trunk/) used the list of subdirectories with a set of GEMS3K I/O files that can be exported from [GEM-Selektor](https://bitbucket.org/gems4/gems3gui/src/trunk/) code for any chemical system (SysEq record).

To compare template and current DATABR files was used [JSONIODIFF](https://bitbucket.org/gems4/jsoniodiff/src/master/) library - the compare analyzer two key-value files or JSON files and prints the difference.


### How to clone&build GEMS3K-test-tool code

* On Mac OS X or linux or Windows10, open a terminal and type in the command line to download the actual branch. Here we’ll clone a project with a [submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules) in it.

```sh
git clone --recurse-submodules https://bitbucket.org/gems4/gems3k-test-tool.git 
```

* To install dependent libraries navigate to the directory where this README.md file is located and type in terminal:

```sh
sudo ./install-dependencies.sh
```

* To build gems3k-test tool, a typical sequence of commands can be executed in the terminal:

```sh
./build.sh
```

* To run: the gems3k-test tool can be executed in the terminal:

```sh
./run-gems3k-test.sh
```

### How to use GEMS3K-test-tool

```sh
Usage: GEMS3k-test [ option(s) ] -d|--test-from TEST_DIR 
Tool for read, recalculate task and compare for init data
Options:
        -h,     --help		show this help message

        -b,     --difference   		   output all differences between template and source (default)
        -c,     --consist-in   		   output all data from the template not consist in the source

        -a,     --approximately EPS	   approximately equal for floating types
        -e,     --essentially EPS	   essentially equal for floating types
        -at,	--absolute-tolerance EPS   absolute tolerance equal for floating types(default)
        -sd,	--significant-digits EPS   identical significant digits equal for floating types
        -lg,	--log-of-values EPS	   identical significant digits of log of values equal for floating types
        -rd,	--rules-document JSONF     structured document describing data to compare

        -md,	--multi-dump		   Multi dump debugging print out
```



###  The 'Difference Rules' document  

To specify different floating-point compare methods for different arrays used [difference template file](template_diff.md) - structured document describing data to compare.
[dbr_diff.json](dbr_diff.json) for example used to compare standalone results with exported from GEM-Selektor code.


### Test suites

* **data-GUI** used to compare standalone results with exported from GEM-Selektor code
To run test, use command:

```sh
> ./gems3k-test -rd  data-GUI/dbr_diff.json -a  1e-7 -d data-GUI
```

* **data** used to compare with previous standalone results
To run test, use command:

```sh
> ./gems3k-test -a  1e-16 -d data
```

To prepare data, use the kva2json tool. Generate the data test from the GUI test:
```sh
> ./kva2json -j -c -i diff/data-GUI/Neutral-old/Neutral-dat.lst -e diff/data/Neutral-old/Neutral-dat.lst
> ./kva2json -j -c -i diff/data-GUI/Solvus1-json/series1-dat.lst -e diff/data/Solvus1-json/series1-dat.lst
> ./kva2json -j -c -i diff/data-GUI/Kaolinite-json/pHtitr-dat.lst -e diff/data/Kaolinite-json/pHtitr-dat.lst
> ./kva2json -t -c -i diff/data-GUI/Kaolinite-kv/pHtitr-dat.lst -e diff/data/Kaolinite-kv/pHtitr-dat.lst
```

* **data-FUN** test using ThermoFun as option in GEMS3K

```sh
> ./gems3k-test -rd  data-FUN/dbr_diff.json -a  1e-7 -d data-FUN
```
