#!/bin/bash

#./install-dependencies.sh

threads=3
BuildType=Debug

mkdir -p build
cd build
cmake .. -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_BUILD_TYPE=$BuildType
make -j $threads 
